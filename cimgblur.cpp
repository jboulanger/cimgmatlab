/*-----------------------------------------------------------------------
  File : cimgblur.cpp

  Description: Blur image using CImg library

  The corresponding m-file is cimgblur.m

  Copyright : Jerome Boulanger
  This software is governed by the Gnu General Public License
  see http://www.gnu.org/copyleft/gpl.html

  for the compilation: using the mex utility provided with matlab, just
  remember to add the -I flags with paths to CImg.h and/or cimgmatlab.h.
  The default lcc cannot be used, it is a C compiler and not a C++ one!
  --------------------------------------------------------------------------*/

#include <mex.h>

#include "mextypes.h"

#define cimg_plugin "matlab.h"

#include "CImg.h"
using namespace cimg_library;

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

  if (nrhs < 2) mexErrMsgTxt("not enough input arguments.");
  if (nrhs > 5) mexErrMsgTxt("too many input arguments.");

  try {

    CImg<> sigma;
    if (mxGetNumberOfElements(prhs[1]) == 1) {
      sigma.assign(3, 1, 1, 1, (float)mxGetScalar(prhs[1]));
    } else {
      sigma.assign(prhs[1], false);
      sigma.unroll('x');
      if (sigma.width() < 3) {
	sigma.resize(3,1,1,1,0,0);
      }
    }

    bool
      gauss = (nrhs < 3 ? false : (bool)mxGetScalar(prhs[2])),
      bc    = (nrhs < 4 ? false : (bool)mxGetScalar(prhs[3])),
      vdata = (nrhs < 5 ? false : (bool)mxGetScalar(prhs[4]));

    CImg<> u(prhs[0], vdata);

    u.blur(sigma(0), sigma(1), sigma(2), gauss, bc);
    plhs[0] = u.toMatlab();

  } catch (CImgException e) {
    mexErrMsgTxt(e.what());
  }

  return;
}
