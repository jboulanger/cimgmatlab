% compilation of mex files
% You may need to adapt the path where to look for libraries
% Jerome Boulanger 2016

disp('Compiling')
clear cimgblur cimgload cimgsave cimgdisplay cimgprox_schatten
if ispc() % windows (assume a mingw mexopts file)
  mex -f mexopts_mingw64.bat -I./ cimgblur.cpp
  mex -f mexopts_mingw64.bat -I./ cimgmedian.cpp
  mex -f mexopts_mingw64.bat -I./ -ltiff cimgload.cpp
  mex -f mexopts_mingw64.bat -I./ -ltiff cimgsave.cpp
  mex -f mexopts_mingw64.bat -I./ cimgprox_schatten.cpp
  mex -f mexopts_mingw64.bat -I./ cimgsegmentcells.cpp
else
    if exist('OCTAVE_VERSION')
        mex -I./ -Wno-date-time -Dcimg_display=0 -Dcimg_use_openmp -lgomp -lpthread cimgblur.cpp
        mex -I./ -Wno-date-time -Dcimg_display=0 -Dcimg_use_openmp -lgomp -lpthread cimgmedian.cpp
        mex -I./ -Wno-date-time -Dcimg_display=0 -Dcimg_use_tiff -ltiff cimgload.cpp
        mex -I./ -Wno-date-time -Dcimg_display=0 -Dcimg_use_tiff -ltiff cimgsave.cpp
        mex -I./ -Wno-date-time -Dcimg_display=1 -lX11 -lpthread cimgdisplay.cpp
        mex -I./ -Wno-date-time -Dcimg_display=0 -Dcimg_use_openmp -Dcimg_use_lapack -lblas -llapack -lm -lgomp -lpthread cimgprox_schatten.cpp        
        mex -I./ -Wno-date-time -Dcimg_display=0 -Dcimg_use_openmp -lgomp -lpthread cimgsegmentcells.cpp
    else
        mex -I./ -Dcimg_display=0 -Dcimg_use_openmp -lgomp -lpthread cimgblur.cpp
        mex -I./ -Dcimg_display=0 -Dcimg_use_openmp -lgomp -lpthread cimgmedian.cpp
        mex -I./ -Dcimg_display=0 -Dcimg_use_tiff -ltiff cimgload.cpp
        mex -I./ -Dcimg_display=0 -Dcimg_use_tiff -ltiff cimgsave.cpp
        mex -I./ -Dcimg_display=1 -lX11 -lpthread cimgdisplay.cpp       
        mex -I./ -Dcimg_display=0 -Dcimg_use_openmp -Dcimg_use_lapack -lblas -llapack -lgomp -lpthread cimgprox_schatten.cpp        
        mex -I./ -Dcimg_display=0 -Dcimg_use_openmp -lgomp -lpthread cimgsegmentcells.cpp
    end
end
disp('Completed')
