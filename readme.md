# CImgLab

This package defines a set of utility function for Matlab/Octave based
on the [CImg Library](http://cimg.eu/) from David Tschumperle and the
plugin matlab.h from Francois Lauze.

## Installation

To compile use :
```
cimgmake
```
in the matlab/octave terminal and check the newly
built function using :
```
cimgtest
```

You probably will need to adjust the compilation options.

## List of functions
- cimgblur
- cimgdisplay
- cimgload
- cimgsave
- cimgprox_schatten