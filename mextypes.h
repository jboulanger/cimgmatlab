/*
 * Compatibility between matlab and octave
 * to include after mex.h
 *
*/

#ifndef __mextypes_h__
#define __mextypes_h__
#ifndef mex_h
#define mex_h
#include "mexproto.h"
#include "mxarray.h"
#include "mextypes.h"
#define uint8_t unsigned char
#define uint16_t unsigned short
#define uint32_t unsigned int
#define uint64_t unsigned long
#define real64_t double
#define real32_t float
#else
#define uint8_t unsigned char
#define uint16_t unsigned short
#define uint32_t unsigned int
#define uint64_t unsigned long
#define real64_t double
#define real32_t float
#endif
#endif
