% J = cimgblur(I, sigma, gauss, boundary, is_color)
%
% Smooth the array I using the Deriche or Van Vliet recursive filter
%
% Input:
%   I     : an array (up to 4D)
%   sigma : array with the sigma for each dimension
%   gauss : 0/1 Gaussian (van Vliet) or exponential (Deriche) filter
%   boundary : Dirichlet  (0) or Neumann (1) boundary conditions
%   is_color : indicate if the 3rd dimension is a color channel
%
% Jerome Boulanger 2016
