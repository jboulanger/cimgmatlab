/*-----------------------------------------------------------------------
  File : cimgsave.cpp

  Description: Save image using CImg library

  Copyright : Jerome Boulanger
  This software is governed by the Gnu General Public License
  see http://www.gnu.org/copyleft/gpl.html

  for the compilation: using the mex utility provided with matlab, just
  remember to add the -I flags with paths to CImg.h and/or cimgmatlab.h.
  The default lcc cannot be used, it is a C compiler and not a C++ one!
  --------------------------------------------------------------------------*/

#include <mex.h>

#include "mextypes.h"

#define cimg_plugin "matlab.h"
#include "CImg.h"
using namespace cimg_library;

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]){
  if (nrhs < 2) mexErrMsgTxt("not enough input arguments.");
  if (nrhs > 3) mexErrMsgTxt("too many input arguments.");
  try {
    const bool vdata = nrhs < 3 ? false : (bool)mxGetScalar(prhs[2]);
    CImg<> img(prhs[0], vdata);
    const char * str = mxArrayToString(prhs[1]);
    if (str) img.save(str);
    else mexErrMsgTxt("Empty filename");
  } catch (CImgException e) {
    mexErrMsgTxt(e.what());
  }
  return;
}
