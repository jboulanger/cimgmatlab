/*-----------------------------------------------------------------------
  File : cimgload.cpp

  Description: Blur image using CImg library

  The corresponding m-file is cimgblur.m

  Copyright : Jerome Boulanger
  This software is governed by the Gnu General Public License
  see http://www.gnu.org/copyleft/gpl.html

  for the compilation: using the mex utility provided with matlab, just
  remember to add the -I flags with paths to CImg.h and/or cimgmatlab.h.
  The default lcc cannot be used, it is a C compiler and not a C++ one!
--------------------------------------------------------------------------*/

#include <mex.h>

#include "mextypes.h"

#define cimg_plugin "matlab.h"

#include "CImg.h"
using namespace cimg_library;

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

    if (nrhs < 1) mexErrMsgTxt("cimgload: not enough input arguments.");
    if (nrhs > 2) mexErrMsgTxt("cimgload: too many input arguments.");

    try {

      CImg<> img(mxArrayToString(prhs[0]));

      if (nlhs == 1) {
	plhs[0] = img.toMatlab();
      }

    } catch (CImgException e) {
      mexErrMsgTxt(e.what());
    }

    return;
}
