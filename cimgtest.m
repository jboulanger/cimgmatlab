% Test the function of the package

% cimgsave
A = round(1000 * randn(64,64,64));
try
  cimgsave(A, 'tmp.tif', 0);
  disp('cimgsave: ok')
catch
  disp(lasterr)
end

% cimgload
try
  B = cimgload('tmp.tif');
  disp('cimgload: ok')
catch
  disp(lasterr)
end

if max(A(:)-B(:)) == 0
  disp('cimgload: identity test ok')
else
  disp(lasterr)
end

% cimgblur
try
  A = round(1000 * randn(64,64,64));
  B = cimgblur(A, [1 1 1], 1, 1, 0);
  B = cimgblur(A, [1 1 1], 1, 1);
  B = cimgblur(A, [1 1 1], 1);
  B = cimgblur(A, [1 1 1]);
  B = cimgblur(A, 1);
  disp('cimgblur: ok')
catch
  disp(lasterr)
end

% cimgdisplay
try
  B = cimgdisplay(A);
  disp('cimgdisplay: ok')
catch
  disp(lasterr)
end

% cimgprox_schatten
try
   % a 16x16 field of 4x4 random matrices
   A = randn(16,16,16);
   A = cimgprox_schatten(A,4,4,1);
   disp('cimgprox_schatten: ok')
catch
    disp(lasterr)
end

% compare with a 2d matrix
try
    gamma = 0;
    Nr = 2;
    Nc = 2;
    M0 = randn(Nr,Nc);
    [U, S, V] = svd(M0);
    M1 = U * max(0, S - gamma) * V';
    M2 = reshape(M0, 1, 1, 1, Nr*Nc);
    M2 = cimgprox_schatten(M2,Nr,Nc,gamma);
    M2 =  reshape(M2, Nr, Nc);
    error = norm(M2 - M1) / norm(M1);
    if (error < 1e-9)
        disp('cimgprox_schatten: equivalence with svd ok');
    else
        disp('cimgprox_schatten: equivalence with svd failed');
    end
catch
    disp(lasterr)
end
